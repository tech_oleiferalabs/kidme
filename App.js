// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/
import 'react-native-gesture-handler';

// Import React and Component
import React from 'react';
import { useEffect } from 'react';
import { Alert } from 'react-native';
// Import Navigators from React Navigation
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

// Import Screens
import SplashScreen from './Screen/SplashScreen';
import LoginScreen from './Screen/LoginScreen';
import RegisterScreen from './Screen/RegisterScreen';
import DrawerNavigationRoutes from './Screen/DrawerNavigationRoutes';
import StoryDetails from './Screen/Components/StoryDetails'
import TeluguPoems from './Screen/Components/TeluguPoems'
import PoemDetails from './Screen/Components/PoemDetails'
import firebase from 'firebase';
const Stack = createStackNavigator();

const Auth = () => {
  // Stack Navigator for Login and Sign up Screen

  useEffect(()=>{
    Alert.alert('App loadedddd');

    if (!firebase.apps.length) {
      firebase.initializeApp({
        apiKey: "AIzaSyBvJCYrVM4ray2wq8_fSpZbCumMjVhX3Nk",
        authDomain: "kid-me.firebaseapp.com",
        projectId: "kid-me",
        storageBucket: "kid-me.appspot.com",
        messagingSenderId: "1046955976594",
        appId: "1:1046955976594:web:ff06d923a9868f0286834b",
        measurementId: "G-75T5VMBNJM"
      });
   }else {
      firebase.app(); // if already initialized, use that one
   }

    
    

  });
  
  return (
    <Stack.Navigator initialRouteName="LoginScreen">
      <Stack.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="RegisterScreen"
        component={RegisterScreen}
        options={{
          title: 'Register', //Set Header Title
          headerStyle: {
            backgroundColor: '#307ecc', //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }}
      />
    </Stack.Navigator>
  );
};

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="SplashScreen">
        {/* SplashScreen which will come once for 5 Seconds */}
        <Stack.Screen
          name="SplashScreen"
          component={SplashScreen}
          // Hiding header for Splash Screen
          options={{headerShown: false}}
        />
        {/* Auth Navigator: Include Login and Signup */}
        <Stack.Screen
          name="Auth"
          component={Auth}
          options={{headerShown: false}}
        />
        {/* Navigation Drawer as a landing page */}
        <Stack.Screen
          name="DrawerNavigationRoutes"
          component={DrawerNavigationRoutes}
          // Hiding header for Navigation Drawer
          options={{headerShown: false}}
        />
        <Stack.Screen name="StoryDetails" component={StoryDetails} />
        <Stack.Screen name="TeluguPoems" component={TeluguPoems} />
        <Stack.Screen name="PoemDetails" component={PoemDetails} />
        
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;