// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React, {useState, useEffect} from 'react';
import {
  ActivityIndicator,
  View,
  StyleSheet,
  Image,
  Alert
} from 'react-native';
import firebase from 'firebase';

import AsyncStorage from '@react-native-community/async-storage';

const SplashScreen = ({navigation}) => {
  //State for ActivityIndicator animation
  const [animating, setAnimating] = useState(true);


  useEffect( () => {
Alert.alert('Splash loaded !!!');


if (!firebase.apps.length) {
  firebase.initializeApp({
    apiKey: "AIzaSyBvJCYrVM4ray2wq8_fSpZbCumMjVhX3Nk",
    authDomain: "kid-me.firebaseapp.com",
    projectId: "kid-me",
    storageBucket: "kid-me.appspot.com",
    messagingSenderId: "1046955976594",
    appId: "1:1046955976594:web:ff06d923a9868f0286834b",
    measurementId: "G-75T5VMBNJM"
  });
}else {
  firebase.app(); // if already initialized, use that one
}



    setTimeout(() => {
      setAnimating(false);
      //Check if user_id is set or not
      //If not then send for Authentication
      //else send to Home Screen
      AsyncStorage.getItem('user_id').then((value) =>
        navigation.replace(
          value === null ? 'Auth' : 'DrawerNavigationRoutes'
        ),
      );
    }, 5000);
  }, []);

  return (
    <View style={styles.container}>
      <Image
        source={require('../Image/kid-me.jpeg')}
        style={{width: '90%', resizeMode: 'contain', margin: 30}}
      />
      <ActivityIndicator
        animating={animating}
        color="#FFFFFF"
        size="large"
        style={styles.activityIndicator}
      />
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'green',
  },
  activityIndicator: {
    alignItems: 'center',
    height: 80,
  },
});