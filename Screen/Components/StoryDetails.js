
import React from 'react';
import { useEffect } from 'react';
import { StyleSheet, Text, View} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import Tts from 'react-native-tts';
const StoryDetails = ({ navigation, route }) => {

    useEffect(() => {
        Tts.voices().then(voices => console.log(voices));
         Tts.setDefaultPitch(2.0);
         Tts.speak(route.params.data.content,{
             iosVoiceId: 'com.apple.ttsbundle.Daniel-compact',
             rate: 0.4,
            
        });

    });

    return (

<View style={styles.container}>

        <View >
        <ScrollView>
            <View style={styles.header}>
                <Text style={{
              fontSize: 20,
              textAlign: 'justify',
              padding:10
              
            }}>{route.params.data.content}</Text>
            </View>
            </ScrollView>

            

            <View style={styles.footer}>
            <Text
            style={{
              fontSize: 18,
              textAlign: 'center',
              color: 'yellow',
             
            }}>
            Moral Stories for Kids by
        </Text>
          <Text
            style={{
              fontSize: 16,
              textAlign: 'center',
              color: 'yellow',
            }}>
            www.oleiferalabs.com
        </Text>
            </View>
        </View>

</View>

    );
}
export default StoryDetails;
const styles = StyleSheet.create({


    container: {
        flex: 1,
        backgroundColor: '#307ecc',

    },
    header: {
        flex: 2,
       
    },
    moral: {
        flex: 1,
        flexDirection:'column',
        backgroundColor: 'white',
        borderTopLeftRadius: 30,
        borderTopRightRadius:30 ,
       
    },
    footer:{
        flex:0.5,
        backgroundColor: 'grey',
        borderRadius:30,
        
    },
    footerText:{
        padding:20,
        marginVertical:20
    },
    listItem:{
        flex: 1,
        flexDirection:'row',
    }



});