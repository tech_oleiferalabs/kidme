
import React from 'react';
import { useEffect } from 'react';
import { ScrollView } from 'react-native-gesture-handler';
import { useState } from 'react';
import Footer from '../Components/Footer'
import { View, Text, SafeAreaView, StyleSheet,FlatList,TouchableOpacity,Image} from 'react-native';

const TeluguPoems = ({ navigation, route }) => {
    console.log('route data is  test 4 :::');

    const getStory = (data) => {
        console.log('Poem data !!!');
        console.log(data);
        navigation.navigate('PoemDetails',{data})
    }

    //const [storyNames, setStoryNames] = useState([]);
    //console.log(route.params.poems.name);
    useEffect(() => {
      
    console.log('page loaded');
    });

    return (

<View style={styles.container}>

<SafeAreaView style={{ flex: 4 }}>
      <ScrollView style={styles.scrollView}>
        <View style={{ flex: 3, padding: 16 }}>

         


<FlatList
  data={route.params.poems}
  numColumns={1}
  keyExtractor={item => item.name}
  renderItem={( {item}) => (
    <View style={{ 
      flex: 1,
      height: 80,
      borderWidth: 2,
      backgroundColor:'white',
      borderRadius:30,
      margin: 10
    }}>
        <TouchableOpacity style={{flex:1,flexDirection:'row'}} onPress={()=>getStory(item)}>
        <Image
                source={require('../../Image/kidme.jpg')}
                style={{
                  width: '20%',
                  height: 50,
                  resizeMode: 'contain',
                  padding:10,
                  margin:12
                }}
              />
           <View style={{ padding:18}}> 
            <Text > Name: {item.name}</Text>
            </View>
        </TouchableOpacity>

    
    </View>
  )}
/>



          <Footer/>
          
          
        </View>
      </ScrollView>
    </SafeAreaView>

</View>

    );
}
export default TeluguPoems;
const styles = StyleSheet.create({


    container: {
        flex: 1,
        backgroundColor: '#307ecc',

    },
    header: {
        flex: 2,
       
    },
    moral: {
        flex: 1,
        flexDirection:'column',
        backgroundColor: 'white',
        borderTopLeftRadius: 30,
        borderTopRightRadius:30 ,
       
    },
    footer:{
        flex:0.5,
        backgroundColor: 'grey',
        borderRadius:30,
        
    },
    footerText:{
        padding:20,
        marginVertical:20
    },
    listItem:{
        flex: 1,
        flexDirection:'row',
    }



});