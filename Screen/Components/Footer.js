import React from 'react';
import {  View, StyleSheet, Text } from 'react-native';

const Footer = () => {
  return (
    <View style={styles.container}>  
        <Text
            style={{
              fontSize: 18,
              textAlign: 'center',
              color: 'yellow',
            }}>
            Moral Stories for Kids by
        </Text>
          <Text
            style={{
              fontSize: 16,
              textAlign: 'center',
              color: 'yellow',
            }}>
            www.oleiferalabs.com
        </Text>
    </View>  
  );
}
const styles = StyleSheet.create({  
  container: {  
      flex: 1,  
  },  
  item: {  
      padding: 10,  
      fontSize: 18,  
      height: 44,  
  },  
})  

export default Footer;