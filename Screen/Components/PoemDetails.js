
import React from 'react';
import { useEffect } from 'react';
import { StyleSheet, Text, View} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
const PoemDetails = ({ navigation, route }) => {

    console.log(route.params);
    useEffect(() => {
       // Tts.voices().then(voices => console.log(voices));
        // Tts.setDefaultPitch(2.0);
        // Tts.speak(route.params.data.content,{
        //     iosVoiceId: 'com.apple.ttsbundle.Daniel-compact',
        //     rate: 0.4,
            
        // });

    });

    return (

<View style={styles.container}>

        <View >
        <ScrollView>
            <View style={styles.header}>
                <Text style={{
              fontSize: 20,
              textAlign: 'justify',
              padding:10
              
            }}>{route.params.data.name} </Text>

            <View style={styles.child1}>
            <Text>{route.params.data.poem}</Text>
            </View>
            <View style={styles.child2}>
            <Text>{route.params.data.meaning}</Text>
            </View>

            </View>
            </ScrollView>

            

            <View style={styles.footer}>
            <Text
            style={{
              fontSize: 18,
              textAlign: 'center',
              color: 'yellow',
             
            }}>
            Moral Stories for Kids by
        </Text>
          <Text
            style={{
              fontSize: 16,
              textAlign: 'center',
              color: 'yellow',
            }}>
            www.oleiferalabs.com
        </Text>
            </View>
        </View>

</View>

    );
}
export default PoemDetails;
const styles = StyleSheet.create({


    container: {
        flex: 1,
        backgroundColor: '#307ecc',

    },
    header: {
        flex: 2,
       
    },
    child1:{
        flex:0.3,
        backgroundColor:'white',
        padding:10,
        borderRadius:2,
        marginVertical:50,
        borderWidth:5
      }  ,
      child2:{
        flex:2,
        backgroundColor:'white',
        textAlign: 'justify',
        borderRadius:0,
        padding:30
      }  ,
    moral: {
        flex: 1,
        flexDirection:'column',
        backgroundColor: 'white',
        borderTopLeftRadius: 30,
        borderTopRightRadius:30 ,
        textAlign: 'justify'
       
    },
    footer:{
        flex:0.5,
        backgroundColor: 'grey',
        borderRadius:30,
        
    },
    footerText:{
        padding:20,
        marginVertical:20
    },
    listItem:{
        flex: 1,
        flexDirection:'row',
    }



});