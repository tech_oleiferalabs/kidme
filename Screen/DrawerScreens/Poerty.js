// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { View, Text, SafeAreaView, StyleSheet,ScrollView,FlatList,TouchableOpacity,Image} from 'react-native';
import firebase from 'firebase';
import Footer from '../Components/Footer'
const Poetry = ({ navigation: { navigate } }) => {
    const db = firebase.firestore();
  const [storyNames, setStoryNames] = useState([]);

  const getStory = (data) => {
    //Alert.alert('Selected story is ' + data.content);
    //console.log(data)
    let poems = [];
    console.log(data.id);
    db.collection("telugu-poems").doc(data.id).collection("poems").get()
    .then(querySnapshot => {
        querySnapshot.forEach(doc1 => {
            console.log(doc1.id, " => " +doc1.data().name);
           //actaulaData.prototype.poems = doc1;
           poems.push(doc1.data());
        });
        //obj.poems(poems);
    }).then(() =>{
        //console.log("Poet  test1:::" +data.name);
        //console.log(poems);
         navigate('TeluguPoems',{poems})
    });



  }

  //useEffect();
  useEffect(() => {
    

    if (!firebase.apps.length) {
      firebase.initializeApp({});
   }else {
      firebase.app(); // if already initialized, use that one
   }

   

   

    loadStories();
    async function loadStories(){
     
      const contacts =  db.collection('telugu-poems');
      const contactsData =  await contacts.get();
      const data = [];
      contactsData.forEach( doc => {

        var  poet =new Object();
        poet.id = doc.id;
        poet.content = doc.data();
        data.push(poet);
        
      });
      //console.log(data);
      //console.log(poems);
      setStoryNames(data);
    }
    
  }, []);
  return (
    

      
<View>
<FlatList
  data={storyNames}
  numColumns={1}
  keyExtractor={item => item.name}
  renderItem={( {item}) => (
    <View style={{ 
      flex: 1,
      height: 80,
      borderWidth: 2,
      backgroundColor:'white',
      borderRadius:30,
      margin: 10
    }}>
        <TouchableOpacity style={{flex:1,flexDirection:'row'}} onPress={()=>getStory(item)}>

        <Image
                source={require('../../Image/kidme.jpg')}
                style={{
                  width: '20%',
                  height: 50,
                  resizeMode: 'contain',
                  padding:10,
                  margin:12
                }}
              />
           <View style={{ padding:18}}> 
            <Text > Name: {item.content.name.toUpperCase()} Padyalu</Text>
            <Text > Period : {item.content.period}th  </Text>
            </View>
        </TouchableOpacity>

    
    </View>
  )}
/>
<Footer/>
</View>



        
          
          
       
  );
};

export default Poetry;

const styles = StyleSheet.create({

  MainContainer: {
    flex: 3,
    //flexDirection: 'row',
    margin: 10
  },

  TextStyle: {
    fontSize: 25,
    textAlign: 'center',
    color: '#ff0000',
    //flex:1,
    width: 120,
    height: 120,
    backgroundColor: 'cyan',


  },
  scrollView: {
    backgroundColor: 'pink'
  },
  item: {
    flex: 1,
    height: 160,
    margin: 1,
    fontSize: 18,
    textAlign: 'center'
  },
  list: {
    flex: 1
  }


});