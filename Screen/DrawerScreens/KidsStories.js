// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { View, Text, SafeAreaView, StyleSheet,ScrollView,FlatList,TouchableOpacity,Image} from 'react-native';
import firebase from 'firebase';
import Footer from '../Components/Footer'
const KidsStories = ({ navigation: { navigate } }) => {

  const [storyNames, setStoryNames] = useState([]);

  const getStory = (data) => {
    //Alert.alert('Selected story is ' + data.content);
    navigate('StoryDetails',{data})

  }

  //useEffect();
  useEffect(() => {
    

    if (!firebase.apps.length) {
      firebase.initializeApp({});
   }else {
      firebase.app(); // if already initialized, use that one
   }

    


    loadStories();
    async function loadStories(){
      const db = firebase.firestore();
      //console.log(db);
      const contacts =  db.collection('kid-stories');
      const contactsData =  await contacts.get();
      const data = [];
      //console.log(contactsData)
      contactsData.forEach(doc => {
        data.push(doc.data())
        //console.log(doc.id, '=>', doc.data());
        //Alert.alert(doc.id);
      });
      console.log(data);
      setStoryNames(data);
    }
    
  }, []);
  return (
    <SafeAreaView style={{ flex: 4 }}>
      <ScrollView style={styles.scrollView}>
        <View style={{ flex: 3, padding: 16 }}>

         


<FlatList
  data={storyNames}
  numColumns={1}
  keyExtractor={item => item.name}
  renderItem={( {item}) => (
    <View style={{ 
      flex: 1,
      height: 80,
      borderWidth: 2,
      backgroundColor:'white',
      borderRadius:30,
      margin: 10
    }}>
        <TouchableOpacity style={{flex:1,flexDirection:'row'}} onPress={()=>getStory(item)}>

        <Image
                source={require('../../Image/kidme.jpg')}
                style={{
                  width: '20%',
                  height: 50,
                  resizeMode: 'contain',
                  padding:10,
                  margin:12
                }}
              />
           <View style={{ padding:18}}> 
            <Text > Name: {item.name}</Text>
            <Text > For all people</Text>
            </View>
        </TouchableOpacity>

    
    </View>
  )}
/>



          <Footer/>
          
          
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default KidsStories;

const styles = StyleSheet.create({

  MainContainer: {
    flex: 3,
    //flexDirection: 'row',
    margin: 10
  },

  TextStyle: {
    fontSize: 25,
    textAlign: 'center',
    color: '#ff0000',
    //flex:1,
    width: 120,
    height: 120,
    backgroundColor: 'cyan',


  },
  scrollView: {
    backgroundColor: 'pink'
  },
  item: {
    flex: 1,
    height: 160,
    margin: 1,
    fontSize: 18,
    textAlign: 'center'
  },
  list: {
    flex: 1
  }


});