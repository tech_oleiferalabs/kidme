
// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React and Component
import React from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { View, Text, SafeAreaView, StyleSheet,Image,FlatList,TouchableOpacity,ScrollView } from 'react-native';
import firebase from 'firebase';
const History = ({ navigation: { navigate } }) => {

  const [storyNames, setStoryNames] = useState([]);

  const getStory = (data) => {
    navigate('StoryDetails',{data})
  }

  //useEffect();
  useEffect(() => {
        if (!firebase.apps.length) {
      firebase.initializeApp({});
   }else {
      firebase.app(); // if already initialized, use that one
   }

   loadStories();
    async function loadStories(){
      const db = firebase.firestore();
      //console.log(db);
      const contacts =  db.collection('history');
      const contactsData =  await contacts.get();
      const data = [];
      //console.log(contactsData)
      contactsData.forEach(doc => {
        data.push(doc.data())
        //console.log(doc.id, '=>', doc.data());
        //Alert.alert(doc.id);
      });
      console.log(data);


      setStoryNames(data);
    }
    
  }, []);
  return (
    


<FlatList
  data={storyNames}
  numColumns={1}
  keyExtractor={item => item.name}
  renderItem={( {item}) => (
    <View style={{ 
      flex: 1,
      height: 80,
      borderWidth: 2,
      backgroundColor:'white',
      borderRadius:30,
      margin: 10
    }}>
        <TouchableOpacity style={{flex:1,flexDirection:'row'}} onPress={()=>getStory(item)}>

        <Image
                source={require('../../Image/kidme.jpg')}
                style={{
                  width: '20%',
                  height: 50,
                  resizeMode: 'contain',
                  padding:10,
                  margin:12
                }}
              />
           <View style={{ padding:18}}> 
            <Text > Name: {item.name}</Text>
            <Text > For all people</Text>
            </View>
        </TouchableOpacity>

    
    </View>
  )}
/>  
       
  );
};

export default History;

const styles = StyleSheet.create({

    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#307ecc',
      },
      image: {
        position: 'absolute', top: 0, left: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'flex-end'
      },

  MainContainer: {
    flex: 3,
    //flexDirection: 'row',
    margin: 10
  },

  TextStyle: {
    fontSize: 25,
    textAlign: 'center',
    color: '#ff0000',
    //flex:1,
    width: 120,
    height: 120,
    backgroundColor: 'white',


  },
  scrollView: {
        backgroundColor: '#307ecc'
  },
  item: {
    flex: 1,
    height: 160,
    margin: 1,
    fontSize: 30,
    textAlign: 'center'
  },
  itemTextStyle:{
    flex: 2,
    height: 160,
    margin: 2,
    fontSize: 20,
    marginBottom:20,
    textAlign: 'center'
  },
  list: {
    flex: 1
  }


});