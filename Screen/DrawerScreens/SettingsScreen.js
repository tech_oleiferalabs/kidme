import React from 'react';
import {  View, FlatList, StyleSheet, Text, Alert,ScrollView,StatusBar } from 'react-native';




const SettingsScreen = () => {
  
  const renderSeparator = () => {  
    return (  
        <View  
            style={{  
                height: 1,  
                width: "100%",  
                backgroundColor: "#000",  
            }}  
        />  
    );  
};  
//handling onPress action  
const getListViewItem = (item) => {  
    Alert.alert(item.key);  
}  


  return (
    
          <View style={styles.container}>  



                <View style={styles.child1}>

                <FlatList  
                    data={[  
                        {key: 'Android'},{key: 'iOS'}, {key: 'Java'},{key: 'Swift'},  
                        {key: 'Php'},{key: 'Hadoop'},{key: 'Sap'},
                        {key: 'Php'},{key: 'Hadoop'},{key: 'Sap'},
                        {key: 'Php'},{key: 'Hadoop'},{key: 'Sap'}
                          
                    ]}  
                    
                    horizontal={true}
                    renderItem={({item}) =>  
                        <Text style={styles.horizontalItem}  
                              onPress={()=>getListViewItem(item)}>{item.key}</Text>}  
                  
                />  

                </View >

                <View style={styles.child2}>
                <FlatList  
                    data={[  
                        {key: 'Android'},{key: 'iOS'}, {key: 'Java'},{key: 'Swift'},  
                        {key: 'Android'},{key: 'iOS'}, {key: 'Java'},{key: 'Swift'},  
                        {key: 'Android'},{key: 'iOS'}, {key: 'Java'},{key: 'Swift'},  
                        {key: 'Android'},{key: 'iOS'}, {key: 'Java'},{key: 'Swift'},  
                        {key: 'Android'},{key: 'iOS'}, {key: 'Java'},{key: 'Swift'},  
                        {key: 'Php'},{key: 'Hadoop'},{key: 'Sap'},
                          
                    ]}  
                    numColumns={3}
                    renderItem={({item}) =>  
                        <Text style={styles.item}  
                              onPress={()=>getListViewItem(item)}>{item.key}</Text>}  
                    ItemSeparatorComponent={renderSeparator}  
                />  
                </View>
            </View>  

  );
}

const styles = StyleSheet.create({  
  container: {  
      flex: 1,  
  },  
  item: {  
      padding: 10,  
      fontSize: 18,  
      height: 44,  
  },
  horizontalItem: {  
    padding: 10,  
    fontSize: 30,  
    height: 64,  
    width:100,
    borderWidth:2,
    borderRadius:20,
    justifyContent:'center'
},
  child1:{
    flex:0.3,
    backgroundColor:'white',
    padding:10,
    borderRadius:2,
    marginVertical:50,
    borderWidth:5
  }  ,
  child2:{
    flex:2,
    backgroundColor:'green',
    borderRadius:30
  }  
})  

export default SettingsScreen;